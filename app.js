const redux = require('redux');

const createStore = redux.createStore;

// set initial state
const initialState = {
    counter : 0
}
// reducer function
const rootReducer = (state=initialState, action) => {
    return state;
}
// create store
const store = createStore(rootReducer);

// get updated state
console.log(store.getState());